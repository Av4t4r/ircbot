#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QTcpSocket>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  QStringList parseText(QString &buffer);
  ~MainWindow();

private:
  Ui::MainWindow *ui;
  QTcpSocket* socket;
  void sendMessage(const QString &message);
private slots:
  void readData();
  void connectToServer();
  void disconnectFromServer();
//  void on_btnConnect_clicked();
  void on_btnSend_clicked();
};

#endif // MAINWINDOW_H
