#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  socket = new QTcpSocket(this);

  //everything works, except for the btnSend signal/slot
  connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
  connect(ui->btnConnect, SIGNAL(clicked()), this, SLOT(connectToServer()));
  connect(ui->btnDisconnect, SIGNAL(clicked()), this, SLOT(disconnectFromServer()));
}

//ugly code. Maybe I'll fix it later
QStringList MainWindow::parseText(QString &buffer){
  QStringList query;
  QString nickname;
  if (buffer.startsWith(":")){
      for (int i = 1; i < buffer.size(); ++i){
          if (buffer[i] != '!'){
              nickname.append(buffer[i]);
            }
          else{
              break;
            }
        }
      query << ":" << nickname << buffer.split(" :");
    }
  else{
      if (buffer.startsWith("PING")){
          query << "PING" << buffer.split("PING");
          qDebug() << "PING REQUEST! " << endl;
          qDebug() << "query[0] data: " << query[0] << endl;
          qDebug() << "query[1] data: " << query[1] << endl;
        }
    }
  return query;
}

void MainWindow::readData(){
  QString readline = socket->readLine();
  //Todo: do stuff with the line
  QStringList parsedBuffer = parseText(readline);
  if (parsedBuffer[0] == ":"){
      for (int i = 1; i < parsedBuffer.size(); ++i){
      ui->txtLog->append(parsedBuffer[i]);
      }
  }
  else{
      if (parsedBuffer[0] == "PING"){
      sendMessage(parsedBuffer[0] + parsedBuffer[1]);
      qDebug() << "Ping requested. Pong sent" << endl;
      }
   }
  if(socket->canReadLine()) readData();
}

void MainWindow::sendMessage(const QString &message){
  if (message.startsWith("QUIT")){
      qDebug() << "it starts with quit" << endl;
      disconnectFromServer();
  }
  else{
    socket->write(message.toUtf8());
    socket->write("\r\n");
    socket->flush();
    qDebug() << "Socket written" << message << endl;
  }
}

void MainWindow::connectToServer(){
  //connecto the socket to a host, port
  socket->connectToHost(QString("irc.freenode.net"), 6667);
  // send the commands in order per IRC standard.
  // NICK, USER, JOIN
  socket->write("NICK Av4t4rBot_Test \r\n");
  socket->write("USER Av4t4rBot_Test  Av4t4rBot_Test Av4t4rBot_Test :Av4t4rBot_Test \r\n");
  socket->write("JOIN #botwar \r\n");
  //Should be connected by now
  }

void MainWindow::disconnectFromServer(){
  //QUIT is the command, the following text is an optional quit message to be sent
  //on QUIT to the channel you are connected to
    qDebug() << "disconnect called" << endl;
  if (socket->isOpen()){
      qDebug () << "socket is open" << endl;
      socket->write("QUIT :Quit C'ya \r\n");
      socket->flush();
      socket->disconnect();
    }
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_btnSend_clicked()
{
  qDebug() << "btnSend pressed." << endl;
  MainWindow::sendMessage(ui->txtSend->text());
  qDebug() << "Text sent!" << endl;
  ui->txtSend->clear();
}
